package com.baomidou.jobs.admin.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import com.baomidou.jobs.model.JobsLog;

/**
 * 日志信息 Mapper
 *
 * @author jobob
 * @since 2019-05-31
 */
@Mapper
public interface JobsLogMapper extends BaseMapper<JobsLog> {

}
